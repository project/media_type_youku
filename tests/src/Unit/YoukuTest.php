<?php

namespace Drupal\Tests\media_type_youku\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\media_type_youku\Plugin\media\Source\Youku;

/**
 * @coversDefaultClass \Drupal\media_type_youku\Plugin\media\Source\Youku
 * @group media_type_youku
 */
class YoukuTest extends UnitTestCase {

  /**
   * Test valid Youku Post URLs.
   *
   * @covers ::getVideoId
   */
  public function testUrlsAsExpected() {
    $passing_urls = [
      // Youku HTTPS.
      'https://v.youku.com/v_show/id_XNDU4MTc2MDUzMg==.html',
      // Youku Non-HTTPS.
      'http://v.youku.com/v_show/id_XNDU4MTc2MDUzMg==.html',
    ];

    foreach ($passing_urls as $url) {
      $youkuVideoId = Youku::getVideoId($url);
      $this->assertTrue(strlen($youkuVideoId) === 15, 'URL is accepted and ID is 15 characters in length.');
    }
  }

  /**
   * Test all sorts of invalid URLs.
   *
   * @covers ::getVideoId
   */
  public function testUrlsNotExpected() {
    $failing_urls = [
      // Youku missing sub-domain.
      'https://youku.com/v_show/id_XNDU4MTc2MDUzMg==.html',
      // Youku wrong domain entirely.
      'https://www.youtube.com/v_show/id_XNDU4MTc2MDUzMg==.html',
      // Youku ID is too short.
      'https://v.youku.com/v_show/id_4MTUzMg==.html',
      // Youku ID is too long.
      'https://v.youku.com/v_show/id_XNDU4MTc2MDUzMg3z5HT==.html',
      // Youku prefix before ID.
      'https://v.youku.com/v_show/pnid_XNDU4MTc2MDUzMg==.html',
    ];

    foreach ($failing_urls as $url) {
      $youkuVideoId = Youku::getVideoId($url);
      $this->assertFalse(strlen($youkuVideoId) === 15, 'URL is not accepted or ID is not equal to 15 characters in length.');
    }
  }

}
