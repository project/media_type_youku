# Media Type Youku

## Introduction

This module allows a Youku video to be supported and embedded within a site.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

* To install this module, `composer require` it, or place it in your modules
  folder and enable the module. Visit https://www.drupal.org/node/1897420
  for further information.

## Configuration

   1. Navigate to Structure > Media types (admin/structure/media). Add the
      new type by clicking on "Add media type" and selecting "Youku" under
      the media source dropdown.
   2. Once all saved, proceed to Content > Media (admin/content/media) and
      clicking on "Add media". Youku will now exist in the list.

## Maintainers

Current maintainers:

* Peter Wong - https://www.drupal.org/u/peterwcm
* Nathan - https://www.drupal.org/u/nterbogt
* Brett Hoffman - https://www.drupal.org/u/hoffismo
