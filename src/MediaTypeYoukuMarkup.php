<?php

namespace Drupal\media_type_youku;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Render\MarkupTrait;

/**
 * Youku Post URL markup.
 */
class MediaTypeYoukuMarkup implements MarkupInterface {

  use MarkupTrait;

}
