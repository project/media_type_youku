<?php

namespace Drupal\media_type_youku\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'youku_textfield' widget.
 *
 * @FieldWidget(
 *   id = "youku_textfield",
 *   label = @Translation("Youku Post URL"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class YoukuWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['value']['#description'] = $this->t('The Youku Post URL. e.g. https://v.youku.com/v_show/id_{video_id}==.html');

    return $element;
  }

}
