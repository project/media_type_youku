<?php

namespace Drupal\media_type_youku\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\media_type_youku\MediaTypeYoukuMarkup;
use Drupal\media_type_youku\Plugin\media\Source\Youku;

/**
 * Plugin implementation of the 'youku' formatter.
 *
 * @FieldFormatter(
 *   id = "youku",
 *   label = @Translation("Youku"),
 *   description = @Translation("Renders the Youku Post URL string as an embed."),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   }
 * )
 */
class MediaTypeYoukuEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $items->getEntity();
    $source = $media->getSource();
    $element = [];

    if ($source instanceof Youku) {
      foreach ($items as $delta => $item) {
        $element[$delta] = [
          '#theme' => 'media_type_youku',
          '#id' => MediaTypeYoukuMarkup::create($source->getMetadata($media, 'embed_html')),
        ];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() === 'media';
  }

}
