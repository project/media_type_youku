<?php

namespace Drupal\media_type_youku\Plugin\MediaDuplicatesChecksum;

use Drupal\media_duplicates\Plugin\MediaDuplicatesChecksumBase;
use Drupal\media\Entity\Media;

/**
 * Youku duplicates checksum.
 *
 * @MediaDuplicatesChecksum(
 *   id = "youku",
 *   label = @Translation("Youku"),
 *   media_types = {"youku"},
 * )
 */
class Youku extends MediaDuplicatesChecksumBase {

  /**
   * {@inheritdoc}
   */
  public function getChecksumData(Media $media) {
    $source = $media->getSource();
    return $source->getSourceFieldValue($media);
  }

}
