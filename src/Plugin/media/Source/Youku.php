<?php

namespace Drupal\media_type_youku\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Provides a media source plugin for Youku.
 *
 * @MediaSource(
 *   id = "youku",
 *   label = @Translation("Youku"),
 *   description = @Translation("Provides a media source plugin for Youku."),
 *   allowed_field_types = {
 *     "string",
 *   },
 *   default_thumbnail_filename = "media-type-youku.png"
 * )
 */
class Youku extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'url' => $this->t('URL'),
      'embed_html' => $this->t('Embed HTML'),
    ];
  }

  /**
   * Runs preg_match on URL.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media object.
   *
   * @return string|false
   *   The Youku video url or FALSE if there is no field or it contains invalid
   *   data.
   */
  protected function getYoukuUrl(MediaInterface $media) {
    if (empty($this->configuration['source_field'])) {
      return FALSE;
    }

    $source_field = $this->configuration['source_field'];

    if (!$media->hasField($source_field)) {
      return FALSE;
    }

    $property_name = $media->{$source_field}->first()->mainPropertyName();
    $data = $media->{$source_field}->{$property_name};

    return static::getUrlComponent($data);
  }

  /**
   * Extract a valid Youku Video ID from a string.
   *
   * @param string $data
   *   The Youku Post URL.
   *
   * @return string|bool
   *   The video ID, or FALSE if it is invalid.
   */
  public static function getVideoId($data) {
    return static::getUrlComponent($data, 'id');
  }

  /**
   * Get a component from the URL.
   *
   * @param string $data
   *   The Youku Post URL.
   * @param string $component
   *   The component from the pattern to return.
   *
   * @return string|bool
   *   The video ID, a valid URL, or FALSE if it is invalid.
   */
  protected static function getUrlComponent($data, string $component = '') {
    $data = trim($data);
    $pattern = '/^http(s)?:\/\/(www\.)?v.youku\.com\/v_show\/id_((?<id>[a-zA-Z\d]{15}))==\.html/i';

    if (preg_match($pattern, $data, $matches)) {
      return isset($matches[$component]) ? $matches[$component] : $data;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $video_url = $this->getYoukuUrl($media);

    if ($video_url === FALSE) {
      return FALSE;
    }

    switch ($attribute_name) {
      case 'url':
        return $video_url;

      case 'embed_html':
        return static::getVideoId($video_url);

      case 'thumbnail_uri':
        return parent::getMetadata($media, 'thumbnail_uri');

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'youku',
      'label' => 'visually_hidden',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFormDisplay(MediaTypeInterface $type, EntityFormDisplayInterface $display) {
    parent::prepareFormDisplay($type, $display);
    $source_field = $this->getSourceFieldDefinition($type)->getName();

    $display->setComponent($source_field, [
      'type' => 'youku_textfield',
      'weight' => $display->getComponent($source_field)['weight'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    $plugin_definition = $this->getPluginDefinition();

    $label = (string) $this->t('@type Post URL', [
      '@type' => $plugin_definition['label'],
    ]);
    return parent::createSourceField($type)->set('label', $label);
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return ['MediaTypeYoukuEmbedCode' => []];
  }

}
