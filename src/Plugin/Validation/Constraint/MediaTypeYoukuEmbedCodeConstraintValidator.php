<?php

namespace Drupal\media_type_youku\Plugin\Validation\Constraint;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media_type_youku\Plugin\media\Source\Youku;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the MediaTypeYoukuEmbedCode constraint.
 */
class MediaTypeYoukuEmbedCodeConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $data = [];
    if (is_string($value)) {
      $data[] = $value;
    }
    elseif ($value instanceof FieldItemInterface) {
      $class = get_class($value);
      $property = $class::mainPropertyName();
      if ($property) {
        $data[] = $value->{$property};
      }
    }
    elseif ($value instanceof FieldItemListInterface) {
      foreach ($value as $item_value) {
        $class = get_class($item_value);

        if (method_exists($class, 'mainPropertyName')) {
          $property = $class::mainPropertyName();
          if ($property) {
            $data[] = $item_value->{$property};
          }
        }
      }
    }

    if ($data) {
      foreach ($data as $item_data) {
        if (Youku::getVideoId($item_data) === FALSE) {
          $this->context->addViolation($constraint->message);
          return;
        }
      }
    }
  }

}
