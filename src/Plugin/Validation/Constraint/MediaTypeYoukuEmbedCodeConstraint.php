<?php

namespace Drupal\media_type_youku\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks to see if the input is a valid Youku Post URL.
 *
 * @Constraint(
 *   id = "MediaTypeYoukuEmbedCode",
 *   label = @Translation("Youku embed code", context = "Validation"),
 *   type = { "link", "string", "string_long" }
 * )
 */
class MediaTypeYoukuEmbedCodeConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'Invalid Youku Post URL. Input should be in the format of "https://v.youku.com/v_show/id_{video_id}==.html".';

}
